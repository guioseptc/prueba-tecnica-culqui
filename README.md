
# Prueba Técnica Desarrollador Backend Senior - Culqui

Esta es una versión inicial del reto planteado por Culqui para el puesto Desarrollador Backend Senior


## Pre-requisitos

Debe tener instalada las siguientes versiones de los programas

* Node 16.x o superior
* npm 8.1
* Typescript 4.x o superior, que puede ser instalado globalmente usando ### `npm install -g typescript` o localmente en el proyecto usando ### `npm install typescript --save-dev`

Para ejecutar un proyecto de NodeJs de manera local debemos garantizar que se cumplan con los pre-requisitos mencionados.


## Comandos para levantar el proyecto

### `npm install` o `npm i`
Primero instalamos todas las dependencias del proyecto

### `npm run start`
Iniciamos el servidor de desarrollo en una terminal


## Autor

Guiosep Tunqui C.

* [Linkedin](https://www.linkedin.com/in/gtunqui/)
* [Gitlab](https://gitlab.com/guioseptc)
