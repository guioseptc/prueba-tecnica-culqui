import redis from "redis";

export class RedisClient {
    // @ts-ignore
    private client: redis.RedisClient;// Creamos un cliente de Redis

    constructor() {
        // Inicializamos el cliente
        this.client = redis.createClient();
    }

    public get(key: string): Promise<string | null> {
        return new Promise((resolve, reject) => {
            // Pedimos el valor de una llave en Redis
            this.client.get(key, (err, value) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(value);
                }
            });
        });
    }

    public set(key: string, value: string): Promise<void> {
        return new Promise((resolve, reject) => {
            // Establecemos un valor para una llave en Redis
            this.client.set(key, value, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
}
