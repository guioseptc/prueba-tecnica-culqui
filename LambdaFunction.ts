// Importamos las clases que vamos a utilizar en nuestra función Lambda
import DynamoDBClient from "./DynamoDBClient";
import { LCR } from "./LCR";
import { RedisClient } from "./RedisClient";
import SQSClient from "./SQSClient";

// Definimos nuestra clase LambdaFunction
export class LambdaFunction {
    // Declaramos nuestras variables de instancia
    private redisClient: RedisClient;
    private dynamoDBClient: DynamoDBClient;
    private sqsClient: SQSClient;

    // Definimos nuestro constructor, inicializamos nuestras variables de instancia
    constructor() {
        this.redisClient = new RedisClient();
        this.dynamoDBClient = new DynamoDBClient();
        this.sqsClient = new SQSClient();
    }

    // Definimos nuestro método run que se encargará de ejecutar nuestra función Lambda
    public async run(): Promise<void> {
        // Definimos los parámetros para recibir un mensaje de una cola SQS
        const params = {
            QueueUrl: 'URL_de_la_cola',
            MaxNumberOfMessages: 1
        };
        // Recibimos el mensaje de la cola SQS
        const data = await this.sqsClient.receiveMessage(params);
        // Obtenemos los mensajes recibidos
        const messages = data.Messages;
        // Si no hay mensajes, salimos del método
        if (!messages || messages.length === 0) {
            return;
        }
        // Obtenemos los nodos, aristas y nodo inicial del mensaje recibido
        const { nodes, edges, initialNode } = JSON.parse(messages[0].Body);
        // Creamos una clave para buscar en Redis
        const key = `${nodes}:${JSON.stringify(edges)}:${initialNode}`;
        // Buscamos en Redis si existe un valor para la clave
        const cached = await this.redisClient.get(key);

        // Si encontramos un valor en Redis, lo utilizamos y eliminamos el mensaje de la cola
        if (cached) {
            console.log(`Cache hit for key ${key}`);
            await this.sqsClient.deleteMessage({
                QueueUrl: 'URL_de_la_cola',
                ReceiptHandle: messages[0].ReceiptHandle
            });
            return;
        }

        // Si no encontramos un valor en Redis, realizamos el cálculo de LCR
        const lcr = new LCR(nodes, edges, initialNode);
        const result = await lcr.run();

        // Guardamos el resultado en Redis
        await this.redisClient.set(key, JSON.stringify(result));
        // Guardamos el resultado en DynamoDB
        await this.dynamoDBClient.putItem({
            TableName: 'nombre_de_la_tabla',
            Item: {
                id: key,
                result: result.toString() // Convertimos el array de números en un string
            }
        });
        // Eliminamos el mensaje de la cola SQS porque después de procesar el mensaje y almacenar el resultado en Redis y DynamoDB, ya no es necesario retenerlo en la cola
        await this.sqsClient.deleteMessage({
            QueueUrl: 'URL_de_la_cola',
            ReceiptHandle: messages[0].ReceiptHandle
        });
    }
}
