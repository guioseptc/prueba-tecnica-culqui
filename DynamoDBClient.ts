import AWS from 'aws-sdk';

class DynamoDBClient {
    // cliente de DynamoDB de AWS
    private client: AWS.DynamoDB.DocumentClient;

    constructor() {
        // se crea un cliente de DynamoDB
        this.client = new AWS.DynamoDB.DocumentClient();
    }

    // función para insertar un ítem en la tabla de DynamoDB
    public async putItem(params: AWS.DynamoDB.DocumentClient.PutItemInput): Promise<void> {
        // se llama al método put de DocumentClient para insertar el ítem en la tabla y se espera a que se resuelva la promesa
        await this.client.put(params).promise();
    }

}

export default DynamoDBClient;
