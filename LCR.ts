export class LCR {
    // Declara tres propiedades privadas: nodes, edges e initialNode
    private nodes: number;
    private edges: [number, number][];
    private initialNode: number;

    // Define un constructor que acepta tres argumentos y asigna los valores correspondientes a las propiedades privadas de la clase
    constructor(nodes: number, edges: [number, number][], initialNode: number) {
        this.nodes = nodes;
        this.edges = edges;
        this.initialNode = initialNode;
    }

    // Define un método asíncrono llamado "run" que devuelve un arreglo de números
    public async run(): Promise<number[]> {
        // Crea un arreglo llamado "positions" con la longitud de "nodes" y llena cada elemento con 0
        let positions = new Array(this.nodes).fill(0);
        // Inicializa "current" con el valor de "initialNode"
        let current = this.initialNode;

        // Repite mientras "true"
        while (true) {
            // Declara una variable "found" con el valor "true"
            let found = true;

            // Itera sobre cada elemento del arreglo "edges"
            for (const edge of this.edges) {
                const [from, to] = edge;

                // Si "from" es igual a "current" y "to" aún no se ha visitado
                if (from === current && positions[to] === 0) {
                    // Cambia el valor de "found" a "false"
                    found = false;
                    // Asigna el valor de "positions[current] + 1" a "positions[to]"
                    positions[to] = positions[current] + 1;
                    // Actualiza "current" con el valor de "to"
                    current = to;
                    // Sale del ciclo "for"
                    break;
                }
            }

            // Si no se encontró ningún nodo adyacente sin visitar, sal del ciclo "while"
            if (found) {
                break;
            }
        }

        // Retorna el arreglo "positions"
        return positions;
    }
}
