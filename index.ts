// importa la clase LambdaFunction del archivo LambdaFunction.ts y lo hace accesible en este archivo
import { LambdaFunction } from "./LambdaFunction";

// crea una instancia de la clase LambdaFunction y la asigna a la variable lambda
const lambda = new LambdaFunction();

// llama el método run() de la instancia de la clase LambdaFunction creada anteriormente
lambda.run();
