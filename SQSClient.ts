import AWS from 'aws-sdk';

// Clase para interactuar con la cola SQS
class SQSClient {
    private client: AWS.SQS;

    constructor() {
        // Crea un nuevo cliente de SQS usando la configuración predeterminada
        this.client = new AWS.SQS();
    }

    // Envía un mensaje a la cola especificada
    public async sendMessage(params: AWS.SQS.SendMessageRequest): Promise<AWS.SQS.SendMessageResult> {
        return await this.client.sendMessage(params).promise();
    }

    // Recibe un mensaje de la cola especificada
    public async receiveMessage(params: AWS.SQS.ReceiveMessageRequest): Promise<AWS.SQS.ReceiveMessageResult> {
        return await this.client.receiveMessage(params).promise();
    }

    // Elimina un mensaje de la cola especificada
    public async deleteMessage(params: AWS.SQS.DeleteMessageRequest): Promise<void> {
        await this.client.deleteMessage(params).promise();
    }
}

export default SQSClient;
